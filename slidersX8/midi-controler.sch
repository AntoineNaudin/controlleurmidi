EESchema Schematic File Version 4
LIBS:midi-controler-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L midi-controler-rescue:POT-RESCUE-midi-controler-midi-controler-rescue RV8
U 1 1 5B59BBF4
P 650 3950
F 0 "RV8" H 650 3850 50  0000 C CNN
F 1 "POT" H 650 3950 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps_RK163_Single_Vertical" H 650 3950 50  0001 C CNN
F 3 "" H 650 3950 50  0000 C CNN
	1    650  3950
	0    1    1    0   
$EndComp
$Comp
L midi-controler-rescue:POT-RESCUE-midi-controler-midi-controler-rescue RV6
U 1 1 5B59BBFA
P 650 2200
F 0 "RV6" H 650 2100 50  0000 C CNN
F 1 "POT" H 650 2200 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps_RK163_Single_Vertical" H 650 2200 50  0001 C CNN
F 3 "" H 650 2200 50  0000 C CNN
	1    650  2200
	0    1    1    0   
$EndComp
$Comp
L midi-controler-rescue:POT-RESCUE-midi-controler-midi-controler-rescue RV5
U 1 1 5B59BC00
P 650 1300
F 0 "RV5" H 650 1200 50  0000 C CNN
F 1 "POT" H 650 1300 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps_RK163_Single_Vertical" H 650 1300 50  0001 C CNN
F 3 "" H 650 1300 50  0000 C CNN
	1    650  1300
	0    1    1    0   
$EndComp
$Comp
L midi-controler-rescue:POT-RESCUE-midi-controler-midi-controler-rescue RV4
U 1 1 5B59BC06
P 600 450
F 0 "RV4" H 600 350 50  0000 C CNN
F 1 "POT" H 600 450 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps_RK163_Single_Vertical" H 600 450 50  0001 C CNN
F 3 "" H 600 450 50  0000 C CNN
	1    600  450 
	0    1    1    0   
$EndComp
$Comp
L midi-controler-rescue:POT-RESCUE-midi-controler-midi-controler-rescue RV7
U 1 1 5B59BC0C
P 650 3050
F 0 "RV7" H 650 2950 50  0000 C CNN
F 1 "POT" H 650 3050 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps_RK163_Single_Vertical" H 650 3050 50  0001 C CNN
F 3 "" H 650 3050 50  0000 C CNN
	1    650  3050
	0    1    1    0   
$EndComp
$Comp
L midi-controler-rescue:4051-midi-controler-rescue U1
U 1 1 5B59BC12
P 3000 2750
F 0 "U1" H 3100 2750 50  0000 C CNN
F 1 "4051" H 3100 2550 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_Socket_LongPads" H 3000 2750 60  0001 C CNN
F 3 "" H 3000 2750 60  0000 C CNN
	1    3000 2750
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR04
U 1 1 5B59BC18
P 3100 3700
F 0 "#PWR04" H 3100 3450 50  0001 C CNN
F 1 "GND" H 3100 3550 50  0000 C CNN
F 2 "" H 3100 3700 50  0000 C CNN
F 3 "" H 3100 3700 50  0000 C CNN
	1    3100 3700
	1    0    0    -1  
$EndComp
Text Label 5300 2350 0    60   ~ 0
out2
Text Label 5300 2250 0    60   ~ 0
gnd2
Text Label 5300 2650 0    60   ~ 0
A2
Text Label 5300 2550 0    60   ~ 0
B2
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR05
U 1 1 5B59BC24
P 1900 7950
F 0 "#PWR05" H 1900 7700 50  0001 C CNN
F 1 "GND" H 1900 7800 50  0000 C CNN
F 2 "" H 1900 7950 50  0000 C CNN
F 3 "" H 1900 7950 50  0000 C CNN
	1    1900 7950
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:PWR_FLAG-midi-controler-rescue #FLG06
U 1 1 5B59BC30
P 3200 1600
F 0 "#FLG06" H 3200 1695 50  0001 C CNN
F 1 "PWR_FLAG" H 3200 1780 50  0000 C CNN
F 2 "" H 3200 1600 50  0000 C CNN
F 3 "" H 3200 1600 50  0000 C CNN
	1    3200 1600
	-1   0    0    1   
$EndComp
$Comp
L midi-controler-rescue:CONN_01X02-midi-controler-rescue P1
U 1 1 5B59BC8D
P 5850 2200
F 0 "P1" H 5850 2350 50  0000 C CNN
F 1 "alim" V 5950 2200 50  0000 C CNN
F 2 "Connectors:PINHEAD1-2" H 5850 2200 50  0001 C CNN
F 3 "" H 5850 2200 50  0000 C CNN
	1    5850 2200
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:POT-RESCUE-midi-controler-midi-controler-rescue RV9
U 1 1 5B59BC94
P 650 5000
F 0 "RV9" H 650 4900 50  0000 C CNN
F 1 "POT" H 650 5000 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps_RK163_Single_Vertical" H 650 5000 50  0001 C CNN
F 3 "" H 650 5000 50  0000 C CNN
	1    650  5000
	0    1    1    0   
$EndComp
$Comp
L midi-controler-rescue:POT-RESCUE-midi-controler-midi-controler-rescue RV10
U 1 1 5B59BC9D
P 650 5800
F 0 "RV10" H 650 5700 50  0000 C CNN
F 1 "POT" H 650 5800 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps_RK163_Single_Vertical" H 650 5800 50  0001 C CNN
F 3 "" H 650 5800 50  0000 C CNN
	1    650  5800
	0    1    1    0   
$EndComp
$Comp
L midi-controler-rescue:POT-RESCUE-midi-controler-midi-controler-rescue RV11
U 1 1 5B59BCA6
P 650 6800
F 0 "RV11" H 650 6700 50  0000 C CNN
F 1 "POT" H 650 6800 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps_RK163_Single_Vertical" H 650 6800 50  0001 C CNN
F 3 "" H 650 6800 50  0000 C CNN
	1    650  6800
	0    1    1    0   
$EndComp
$Comp
L midi-controler-rescue:CONN_01X03-midi-controler-rescue P3
U 1 1 5B59BCB3
P 5850 2550
F 0 "P3" H 5850 2750 50  0000 C CNN
F 1 "IN" V 5950 2550 50  0000 C CNN
F 2 "Connectors:Wafer_Vertical10x5.8x7RM2.5-3" H 5850 2550 50  0001 C CNN
F 3 "" H 5850 2550 50  0000 C CNN
	1    5850 2550
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:C-midi-controler-rescue C2
U 1 1 5B59BCB9
P 5050 2100
F 0 "C2" H 5075 2200 50  0000 L CNN
F 1 "100n" H 5075 2000 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L4.6mm_W3.8mm_P2.50mm_MKS02_FKP02" H 5088 1950 50  0001 C CNN
F 3 "" H 5050 2100 50  0001 C CNN
	1    5050 2100
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:Conn_01x01-midi-controler-rescue P2
U 1 1 5B59BCC6
P 5850 2350
F 0 "P2" H 5850 2450 50  0000 C CNN
F 1 "out" H 5850 2250 50  0000 C CNN
F 2 "Connectors:PINTST" H 5850 2350 50  0001 C CNN
F 3 "" H 5850 2350 50  0001 C CNN
	1    5850 2350
	1    0    0    -1  
$EndComp
Text Label 5300 2450 0    60   ~ 0
C2
Wire Wire Line
	600  700  600  800 
Wire Wire Line
	0    200  0    1050
Wire Wire Line
	600  200  0    200 
Wire Wire Line
	650  1050 0    1050
Connection ~ 0    1050
Wire Wire Line
	650  2800 0    2800
Connection ~ 0    2800
Wire Wire Line
	650  1950 0    1950
Connection ~ 0    1950
Wire Wire Line
	0    3700 650  3700
Connection ~ 0    3700
Wire Wire Line
	-150 -150 -150 800 
Wire Wire Line
	600  800  -150 800 
Connection ~ -150 800 
Wire Wire Line
	650  1550 -150 1550
Connection ~ -150 1550
Wire Wire Line
	650  2450 -150 2450
Connection ~ -150 2450
Wire Wire Line
	650  3300 -150 3300
Connection ~ -150 3300
Wire Wire Line
	650  4200 -150 4200
Connection ~ -150 4200
Wire Wire Line
	2300 2150 1900 2150
Wire Wire Line
	1900 2150 1900 450 
Wire Wire Line
	1900 450  750  450 
Wire Wire Line
	800  1300 1800 1300
Wire Wire Line
	1800 1300 1800 2250
Wire Wire Line
	1800 2250 2300 2250
Wire Wire Line
	2300 2350 850  2350
Wire Wire Line
	850  2350 850  2200
Wire Wire Line
	850  2200 800  2200
Wire Wire Line
	800  3050 1250 3050
Wire Wire Line
	1250 3050 1250 2450
Wire Wire Line
	1250 2450 2300 2450
Wire Wire Line
	2300 2550 1300 2550
Wire Wire Line
	1300 2550 1300 3950
Wire Wire Line
	1300 3950 800  3950
Wire Wire Line
	2300 2650 1400 2650
Wire Wire Line
	1750 5800 1750 2750
Wire Wire Line
	1750 2750 2300 2750
Wire Wire Line
	2300 2850 1900 2850
Wire Wire Line
	1900 2850 1900 6800
Connection ~ -150 5250
Connection ~ 0    6550
Wire Wire Line
	3000 3400 3000 3600
Wire Wire Line
	3100 3600 3100 3700
Wire Wire Line
	3700 3250 3700 3450
Connection ~ 3100 3600
Wire Wire Line
	2300 3050 2050 3050
Wire Wire Line
	2050 3050 2050 3600
Wire Wire Line
	4300 3450 3700 3450
Connection ~ 3700 3450
Wire Wire Line
	2300 3350 2300 4000
Wire Wire Line
	2300 4000 4400 4000
Wire Wire Line
	4400 4000 4400 2450
Wire Wire Line
	4400 2450 5650 2450
Wire Wire Line
	2300 3250 2250 3250
Wire Wire Line
	2250 3250 2250 4050
Wire Wire Line
	2250 4050 4450 4050
Wire Wire Line
	4450 4050 4450 2550
Wire Wire Line
	4450 2550 5650 2550
Wire Wire Line
	2300 3150 2200 3150
Wire Wire Line
	2200 3150 2200 4100
Wire Wire Line
	2200 4100 4500 4100
Wire Wire Line
	4500 4100 4500 2650
Wire Wire Line
	4500 2650 5650 2650
Wire Wire Line
	1900 7900 1900 7950
Wire Wire Line
	-150 -150 3000 -150
Wire Wire Line
	4300 2250 5050 2250
Wire Wire Line
	4300 2250 4300 3450
Connection ~ 3000 3600
Wire Wire Line
	3200 1600 3000 1600
Connection ~ 3000 1600
Wire Wire Line
	0    4750 650  4750
Wire Wire Line
	650  5250 -150 5250
Wire Wire Line
	800  5000 1400 5000
Wire Wire Line
	0    5550 650  5550
Wire Wire Line
	650  6050 -150 6050
Wire Wire Line
	800  5800 1750 5800
Wire Wire Line
	0    6550 650  6550
Wire Wire Line
	650  7050 -150 7050
Wire Wire Line
	1900 6800 800  6800
Connection ~ 0    5550
Connection ~ -150 6050
Connection ~ -150 7050
Wire Wire Line
	1400 5000 1400 2650
Connection ~ 5050 2250
Wire Wire Line
	5050 1950 5050 1800
Wire Wire Line
	5050 1800 5650 1800
Wire Wire Line
	3700 2150 3950 2150
Wire Wire Line
	3950 2150 3950 2350
Connection ~ 0    4750
Wire Wire Line
	0    1050 0    1950
Wire Wire Line
	0    2800 0    3700
Wire Wire Line
	0    1950 0    2800
Wire Wire Line
	0    3700 0    4750
Wire Wire Line
	-150 800  -150 1550
Wire Wire Line
	-150 1550 -150 2450
Wire Wire Line
	-150 2450 -150 3300
Wire Wire Line
	-150 3300 -150 4200
Wire Wire Line
	-150 4200 -150 5250
Wire Wire Line
	-150 5250 -150 6050
Wire Wire Line
	0    6550 0    7900
Wire Wire Line
	3100 3600 3000 3600
Wire Wire Line
	3700 3450 3700 3600
Wire Wire Line
	3000 1600 3000 2100
Wire Wire Line
	0    5550 0    6550
Wire Wire Line
	-150 7050 -150 7550
Wire Wire Line
	5050 2250 5650 2250
Wire Wire Line
	5650 1800 5650 2150
Wire Wire Line
	0    7900 1900 7900
Wire Wire Line
	3100 3600 3700 3600
Wire Wire Line
	3950 2350 5650 2350
Wire Wire Line
	0    4750 0    5550
Wire Wire Line
	-150 6050 -150 7050
Wire Wire Line
	2050 3600 3000 3600
Wire Wire Line
	12900 2400 14250 2400
Wire Wire Line
	10650 3750 11950 3750
Wire Wire Line
	11600 0    11600 2250
Wire Wire Line
	8600 1200 8600 8050
Wire Wire Line
	8450 950  8450 7700
Wire Wire Line
	3000 -150 3000 1600
Wire Wire Line
	-6100 2800 -6100 6150
Wire Wire Line
	-6200 2700 -6200 4800
Wire Wire Line
	-10100 -1150 -4500 -1150
Wire Wire Line
	-8150 -650 -5600 -650
Wire Wire Line
	-3550 2500 -1850 2500
Wire Wire Line
	-1850 1950 -1850 2300
Wire Wire Line
	-3800 3600 -3800 3750
NoConn ~ -9150 -550
NoConn ~ -9150 800 
NoConn ~ -9100 2150
NoConn ~ -9050 3500
NoConn ~ -9000 4900
NoConn ~ -8950 6250
NoConn ~ -8950 7650
NoConn ~ -8900 8900
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR024
U 1 1 5B605DE4
P -3550 2800
F 0 "#PWR024" H -3550 2550 50  0001 C CNN
F 1 "GND" H -3550 2650 50  0000 C CNN
F 2 "" H -3550 2800 50  0000 C CNN
F 3 "" H -3550 2800 50  0000 C CNN
	1    -3550 2800
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:R-midi-controler-rescue R9
U 1 1 5B605AE3
P -3550 2650
F 0 "R9" V -3470 2650 50  0000 C CNN
F 1 "10k" V -3550 2650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -3620 2650 50  0001 C CNN
F 3 "" H -3550 2650 50  0001 C CNN
	1    -3550 2650
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR023
U 1 1 5B5BED55
P -8100 2450
F 0 "#PWR023" H -8100 2200 50  0001 C CNN
F 1 "GND" H -8100 2300 50  0000 C CNN
F 2 "" H -8100 2450 50  0000 C CNN
F 3 "" H -8100 2450 50  0000 C CNN
	1    -8100 2450
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR022
U 1 1 5B5BEAB6
P -8050 3800
F 0 "#PWR022" H -8050 3550 50  0001 C CNN
F 1 "GND" H -8050 3650 50  0000 C CNN
F 2 "" H -8050 3800 50  0000 C CNN
F 3 "" H -8050 3800 50  0000 C CNN
	1    -8050 3800
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR021
U 1 1 5B5BEA06
P -8000 5200
F 0 "#PWR021" H -8000 4950 50  0001 C CNN
F 1 "GND" H -8000 5050 50  0000 C CNN
F 2 "" H -8000 5200 50  0000 C CNN
F 3 "" H -8000 5200 50  0000 C CNN
	1    -8000 5200
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR020
U 1 1 5B5BE956
P -7950 6550
F 0 "#PWR020" H -7950 6300 50  0001 C CNN
F 1 "GND" H -7950 6400 50  0000 C CNN
F 2 "" H -7950 6550 50  0000 C CNN
F 3 "" H -7950 6550 50  0000 C CNN
	1    -7950 6550
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR019
U 1 1 5B5BE8A6
P -7900 9200
F 0 "#PWR019" H -7900 8950 50  0001 C CNN
F 1 "GND" H -7900 9050 50  0000 C CNN
F 2 "" H -7900 9200 50  0000 C CNN
F 3 "" H -7900 9200 50  0000 C CNN
	1    -7900 9200
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR018
U 1 1 5B5BE751
P -7950 7950
F 0 "#PWR018" H -7950 7700 50  0001 C CNN
F 1 "GND" H -7950 7800 50  0000 C CNN
F 2 "" H -7950 7950 50  0000 C CNN
F 3 "" H -7950 7950 50  0000 C CNN
	1    -7950 7950
	0    -1   -1   0   
$EndComp
Connection ~ -10100 7850
Wire Wire Line
	-9650 7850 -10100 7850
Connection ~ -10100 6450
Wire Wire Line
	-9650 6450 -10100 6450
Connection ~ -10100 5100
Wire Wire Line
	-9700 5100 -10100 5100
Connection ~ -10100 2350
Wire Wire Line
	-9800 2350 -10100 2350
Connection ~ -10100 1000
Wire Wire Line
	-9850 1000 -10100 1000
Connection ~ -10100 -350
Wire Wire Line
	-9850 -350 -10100 -350
Wire Wire Line
	-10100 -350 -10100 600 
Wire Wire Line
	-10100 600  -10100 1000
Connection ~ -10100 600 
Wire Wire Line
	-10100 600  -9150 600 
Connection ~ -10100 3700
Wire Wire Line
	-9750 3700 -10100 3700
Wire Wire Line
	-10100 5100 -10100 6050
Wire Wire Line
	-10100 6050 -10100 6450
Connection ~ -10100 6050
Wire Wire Line
	-8950 6050 -10100 6050
Wire Wire Line
	-10100 9100 -9600 9100
Wire Wire Line
	-8900 9100 -9000 9100
$Comp
L midi-controler-rescue:R-midi-controler-rescue R8
U 1 1 5B5BB03A
P -9450 9100
F 0 "R8" V -9370 9100 50  0000 C CNN
F 1 "1k" V -9450 9100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9520 9100 50  0001 C CNN
F 3 "" H -9450 9100 50  0001 C CNN
	1    -9450 9100
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D8
U 1 1 5B5BB034
P -9150 9100
F 0 "D8" H -9150 9200 50  0000 C CNN
F 1 "LED" H -9150 9000 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9150 9100 50  0001 C CNN
F 3 "" H -9150 9100 50  0001 C CNN
	1    -9150 9100
	-1   0    0    1   
$EndComp
NoConn ~ -8900 9300
Wire Wire Line
	-10100 7850 -10100 8700
Wire Wire Line
	-10100 8700 -10100 9100
Connection ~ -10100 8700
Wire Wire Line
	-10100 8700 -8900 8700
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW6
U 1 1 5B5BB02B
P -8400 9000
F 0 "SW6" H -8400 9450 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8400 8550 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8400 9000 60  0001 C CNN
F 3 "" H -8400 9000 60  0001 C CNN
	1    -8400 9000
	-1   0    0    1   
$EndComp
Wire Wire Line
	-8950 7850 -9050 7850
$Comp
L midi-controler-rescue:R-midi-controler-rescue R7
U 1 1 5B5BB01E
P -9500 7850
F 0 "R7" V -9420 7850 50  0000 C CNN
F 1 "1k" V -9500 7850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9570 7850 50  0001 C CNN
F 3 "" H -9500 7850 50  0001 C CNN
	1    -9500 7850
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D7
U 1 1 5B5BB018
P -9200 7850
F 0 "D7" H -9200 7950 50  0000 C CNN
F 1 "LED" H -9200 7750 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9200 7850 50  0001 C CNN
F 3 "" H -9200 7850 50  0001 C CNN
	1    -9200 7850
	-1   0    0    1   
$EndComp
NoConn ~ -8950 8050
Wire Wire Line
	-10100 6450 -10100 7450
Wire Wire Line
	-10100 7450 -10100 7850
Connection ~ -10100 7450
Wire Wire Line
	-10100 7450 -8950 7450
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW7
U 1 1 5B5BB00F
P -8450 7750
F 0 "SW7" H -8450 8200 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8450 7300 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8450 7750 60  0001 C CNN
F 3 "" H -8450 7750 60  0001 C CNN
	1    -8450 7750
	-1   0    0    1   
$EndComp
Wire Wire Line
	-8950 6450 -9050 6450
$Comp
L midi-controler-rescue:R-midi-controler-rescue R6
U 1 1 5B5BB002
P -9500 6450
F 0 "R6" V -9420 6450 50  0000 C CNN
F 1 "1k" V -9500 6450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9570 6450 50  0001 C CNN
F 3 "" H -9500 6450 50  0001 C CNN
	1    -9500 6450
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D6
U 1 1 5B5BAFFC
P -9200 6450
F 0 "D6" H -9200 6550 50  0000 C CNN
F 1 "LED" H -9200 6350 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9200 6450 50  0001 C CNN
F 3 "" H -9200 6450 50  0001 C CNN
	1    -9200 6450
	-1   0    0    1   
$EndComp
NoConn ~ -8950 6650
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW3
U 1 1 5B5BAFF5
P -8450 6350
F 0 "SW3" H -8450 6800 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8450 5900 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8450 6350 60  0001 C CNN
F 3 "" H -8450 6350 60  0001 C CNN
	1    -8450 6350
	-1   0    0    1   
$EndComp
Wire Wire Line
	-9000 5100 -9100 5100
$Comp
L midi-controler-rescue:R-midi-controler-rescue R5
U 1 1 5B5BAC32
P -9550 5100
F 0 "R5" V -9470 5100 50  0000 C CNN
F 1 "1k" V -9550 5100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9620 5100 50  0001 C CNN
F 3 "" H -9550 5100 50  0001 C CNN
	1    -9550 5100
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D5
U 1 1 5B5BAC2C
P -9250 5100
F 0 "D5" H -9250 5200 50  0000 C CNN
F 1 "LED" H -9250 5000 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9250 5100 50  0001 C CNN
F 3 "" H -9250 5100 50  0001 C CNN
	1    -9250 5100
	-1   0    0    1   
$EndComp
NoConn ~ -9000 5300
Wire Wire Line
	-10100 3700 -10100 4700
Wire Wire Line
	-10100 4700 -10100 5100
Connection ~ -10100 4700
Wire Wire Line
	-10100 4700 -9000 4700
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW1
U 1 1 5B5BAC23
P -8500 5000
F 0 "SW1" H -8500 5450 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8500 4550 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8500 5000 60  0001 C CNN
F 3 "" H -8500 5000 60  0001 C CNN
	1    -8500 5000
	-1   0    0    1   
$EndComp
Wire Wire Line
	-9050 3700 -9150 3700
$Comp
L midi-controler-rescue:R-midi-controler-rescue R4
U 1 1 5B5BAC16
P -9600 3700
F 0 "R4" V -9520 3700 50  0000 C CNN
F 1 "1k" V -9600 3700 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9670 3700 50  0001 C CNN
F 3 "" H -9600 3700 50  0001 C CNN
	1    -9600 3700
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D4
U 1 1 5B5BAC10
P -9300 3700
F 0 "D4" H -9300 3800 50  0000 C CNN
F 1 "LED" H -9300 3600 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9300 3700 50  0001 C CNN
F 3 "" H -9300 3700 50  0001 C CNN
	1    -9300 3700
	-1   0    0    1   
$EndComp
NoConn ~ -9050 3900
Wire Wire Line
	-10100 2350 -10100 3300
Wire Wire Line
	-10100 3300 -10100 3700
Connection ~ -10100 3300
Wire Wire Line
	-10100 3300 -9050 3300
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW4
U 1 1 5B5BAC07
P -8550 3600
F 0 "SW4" H -8550 4050 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8550 3150 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8550 3600 60  0001 C CNN
F 3 "" H -8550 3600 60  0001 C CNN
	1    -8550 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	-9100 2350 -9200 2350
$Comp
L midi-controler-rescue:R-midi-controler-rescue R3
U 1 1 5B5BA94E
P -9650 2350
F 0 "R3" V -9570 2350 50  0000 C CNN
F 1 "1k" V -9650 2350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9720 2350 50  0001 C CNN
F 3 "" H -9650 2350 50  0001 C CNN
	1    -9650 2350
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D3
U 1 1 5B5BA948
P -9350 2350
F 0 "D3" H -9350 2450 50  0000 C CNN
F 1 "LED" H -9350 2250 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9350 2350 50  0001 C CNN
F 3 "" H -9350 2350 50  0001 C CNN
	1    -9350 2350
	-1   0    0    1   
$EndComp
NoConn ~ -9100 2550
Wire Wire Line
	-10100 1000 -10100 1950
Wire Wire Line
	-10100 1950 -10100 2350
Connection ~ -10100 1950
Wire Wire Line
	-10100 1950 -9100 1950
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW8
U 1 1 5B5BA93F
P -8600 2250
F 0 "SW8" H -8600 2700 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8600 1800 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8600 2250 60  0001 C CNN
F 3 "" H -8600 2250 60  0001 C CNN
	1    -8600 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	-9150 1000 -9250 1000
$Comp
L midi-controler-rescue:R-midi-controler-rescue R2
U 1 1 5B5BA887
P -9700 1000
F 0 "R2" V -9620 1000 50  0000 C CNN
F 1 "1k" V -9700 1000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9770 1000 50  0001 C CNN
F 3 "" H -9700 1000 50  0001 C CNN
	1    -9700 1000
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D2
U 1 1 5B5BA881
P -9400 1000
F 0 "D2" H -9400 1100 50  0000 C CNN
F 1 "LED" H -9400 900 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9400 1000 50  0001 C CNN
F 3 "" H -9400 1000 50  0001 C CNN
	1    -9400 1000
	-1   0    0    1   
$EndComp
NoConn ~ -9150 1200
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR010
U 1 1 5B5BA87A
P -8150 1100
F 0 "#PWR010" H -8150 850 50  0001 C CNN
F 1 "GND" H -8150 950 50  0000 C CNN
F 2 "" H -8150 1100 50  0000 C CNN
F 3 "" H -8150 1100 50  0000 C CNN
	1    -8150 1100
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW2
U 1 1 5B5BA872
P -8650 900
F 0 "SW2" H -8650 1350 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8650 450 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8650 900 60  0001 C CNN
F 3 "" H -8650 900 60  0001 C CNN
	1    -8650 900 
	-1   0    0    1   
$EndComp
Wire Wire Line
	-9150 -350 -9250 -350
$Comp
L midi-controler-rescue:R-midi-controler-rescue R1
U 1 1 5B5B9575
P -9700 -350
F 0 "R1" V -9620 -350 50  0000 C CNN
F 1 "1k" V -9700 -350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9770 -350 50  0001 C CNN
F 3 "" H -9700 -350 50  0001 C CNN
	1    -9700 -350
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D1
U 1 1 5B5B9407
P -9400 -350
F 0 "D1" H -9400 -250 50  0000 C CNN
F 1 "LED" H -9400 -450 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9400 -350 50  0001 C CNN
F 3 "" H -9400 -350 50  0001 C CNN
	1    -9400 -350
	-1   0    0    1   
$EndComp
NoConn ~ -9150 -150
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR08
U 1 1 5B5B932E
P -8150 -250
F 0 "#PWR08" H -8150 -500 50  0001 C CNN
F 1 "GND" H -8150 -400 50  0000 C CNN
F 2 "" H -8150 -250 50  0000 C CNN
F 3 "" H -8150 -250 50  0000 C CNN
	1    -8150 -250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	-10100 -750 -10100 -350
Wire Wire Line
	-10100 -1150 -10100 -750
Connection ~ -10100 -750
Wire Wire Line
	-10100 -750 -9150 -750
Wire Wire Line
	-5600 8800 -7900 8800
Wire Wire Line
	-7950 7550 -5750 7550
Wire Wire Line
	-7950 6150 -6100 6150
Wire Wire Line
	-8000 4800 -6200 4800
Wire Wire Line
	-6250 3400 -8050 3400
Wire Wire Line
	-6650 2050 -8100 2050
Wire Wire Line
	-5700 700  -8150 700 
Connection ~ -3550 2500
Wire Wire Line
	-3550 2300 -3550 2500
Wire Wire Line
	-3800 2300 -3550 2300
Wire Wire Line
	-2450 1950 -1850 1950
Wire Wire Line
	-2450 2100 -2450 1950
Wire Wire Line
	-3200 2400 -3200 3600
Wire Wire Line
	-3000 2800 -1850 2800
Wire Wire Line
	-3000 4250 -3000 2800
Wire Wire Line
	-5300 4250 -3000 4250
Wire Wire Line
	-5300 3300 -5300 4250
Wire Wire Line
	-5200 3300 -5300 3300
Wire Wire Line
	-3050 2700 -1850 2700
Wire Wire Line
	-3050 4200 -3050 2700
Wire Wire Line
	-5250 4200 -3050 4200
Wire Wire Line
	-5250 3400 -5250 4200
Wire Wire Line
	-5200 3400 -5250 3400
Wire Wire Line
	-3100 2600 -1850 2600
Wire Wire Line
	-3100 4150 -3100 2600
Wire Wire Line
	-5200 4150 -3100 4150
Wire Wire Line
	-5200 3500 -5200 4150
Wire Wire Line
	-3200 3600 -3800 3600
Wire Wire Line
	-4500 950  -4500 2250
Wire Wire Line
	-4500 -1150 -4500 950 
Connection ~ -4500 950 
Wire Wire Line
	-1850 950  -4500 950 
Connection ~ -1850 1950
Wire Wire Line
	-1850 950  -1850 1950
Wire Wire Line
	-5450 3200 -5450 3750
Wire Wire Line
	-5200 3200 -5450 3200
Connection ~ -3800 3600
Wire Wire Line
	-3800 3400 -3800 3600
Wire Wire Line
	-4400 3750 -3800 3750
Connection ~ -4400 3750
Wire Wire Line
	-4400 3750 -4400 3850
Wire Wire Line
	-5450 3750 -4500 3750
Wire Wire Line
	-4400 3750 -4500 3750
Connection ~ -4500 3750
Wire Wire Line
	-4500 3550 -4500 3750
Wire Wire Line
	-5600 3000 -5600 8800
Wire Wire Line
	-5200 3000 -5600 3000
Wire Wire Line
	-5750 2900 -5200 2900
Wire Wire Line
	-5750 7550 -5750 2900
Wire Wire Line
	-5200 2800 -6100 2800
Wire Wire Line
	-5200 2700 -6200 2700
Wire Wire Line
	-6250 2600 -5200 2600
Wire Wire Line
	-6250 2600 -6250 3400
Wire Wire Line
	-6650 2050 -6650 2500
Wire Wire Line
	-6650 2500 -5200 2500
Wire Wire Line
	-5700 2400 -5200 2400
Wire Wire Line
	-5700 700  -5700 2400
Wire Wire Line
	-5600 -650 -5600 2300
Wire Wire Line
	-5600 2300 -5200 2300
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW5
U 1 1 5B5B66E4
P -8650 -450
F 0 "SW5" H -8650 0   70  0000 C CNN
F 1 "SWITCH_DPDT" H -8650 -900 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8650 -450 60  0001 C CNN
F 3 "" H -8650 -450 60  0001 C CNN
	1    -8650 -450
	-1   0    0    1   
$EndComp
$Comp
L midi-controler-rescue:Conn_01x01-midi-controler-rescue P5
U 1 1 5B5B476D
P -1650 2500
F 0 "P5" H -1650 2600 50  0000 C CNN
F 1 "out" H -1650 2400 50  0000 C CNN
F 2 "Connectors:PINTST" H -1650 2500 50  0001 C CNN
F 3 "" H -1650 2500 50  0001 C CNN
	1    -1650 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	-2450 2400 -1850 2400
Wire Wire Line
	-3200 2400 -2450 2400
Connection ~ -2450 2400
$Comp
L midi-controler-rescue:C-midi-controler-rescue C3
U 1 1 5B5B4760
P -2450 2250
F 0 "C3" H -2425 2350 50  0000 L CNN
F 1 "100n" H -2425 2150 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L4.6mm_W3.8mm_P2.50mm_MKS02_FKP02" H -2412 2100 50  0001 C CNN
F 3 "" H -2450 2250 50  0001 C CNN
	1    -2450 2250
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:CONN_01X03-midi-controler-rescue P6
U 1 1 5B5B475A
P -1650 2700
F 0 "P6" H -1650 2900 50  0000 C CNN
F 1 "IN" V -1550 2700 50  0000 C CNN
F 2 "Connectors:Wafer_Vertical10x5.8x7RM2.5-3" H -1650 2700 50  0001 C CNN
F 3 "" H -1650 2700 50  0000 C CNN
	1    -1650 2700
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:CONN_01X02-midi-controler-rescue P4
U 1 1 5B5B4734
P -1650 2350
F 0 "P4" H -1650 2500 50  0000 C CNN
F 1 "alim" V -1550 2350 50  0000 C CNN
F 2 "Connectors:PINHEAD1-2" H -1650 2350 50  0001 C CNN
F 3 "" H -1650 2350 50  0000 C CNN
	1    -1650 2350
	1    0    0    -1  
$EndComp
Text Label -2250 950  0    60   ~ 0
+5v2
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR07
U 1 1 5B5B46B5
P -4400 3850
F 0 "#PWR07" H -4400 3600 50  0001 C CNN
F 1 "GND" H -4400 3700 50  0000 C CNN
F 2 "" H -4400 3850 50  0000 C CNN
F 3 "" H -4400 3850 50  0000 C CNN
	1    -4400 3850
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:4051-midi-controler-rescue U2
U 1 1 5B5B46AF
P -4500 2900
F 0 "U2" H -4400 2900 50  0000 C CNN
F 1 "4051" H -4400 2700 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_Socket_LongPads" H -4500 2900 60  0001 C CNN
F 3 "" H -4500 2900 60  0000 C CNN
	1    -4500 2900
	1    0    0    -1  
$EndComp
Text GLabel -7900 -2300 0    50   Input ~ 0
led-PWM
Wire Wire Line
	-6050 14250 -6050 17600
Wire Wire Line
	-6150 14150 -6150 16250
Wire Wire Line
	-10050 10300 -4450 10300
Wire Wire Line
	-8100 10800 -5550 10800
Wire Wire Line
	-3500 13950 -1800 13950
Wire Wire Line
	-1800 13400 -1800 13750
Wire Wire Line
	-3750 15050 -3750 15200
NoConn ~ -9100 10900
NoConn ~ -9100 12250
NoConn ~ -9050 13600
NoConn ~ -9000 14950
NoConn ~ -8950 16350
NoConn ~ -8900 17700
NoConn ~ -8900 19100
NoConn ~ -8850 20350
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC2130
P -3500 14250
F 0 "#PWR?" H -3500 14000 50  0001 C CNN
F 1 "GND" H -3500 14100 50  0000 C CNN
F 2 "" H -3500 14250 50  0000 C CNN
F 3 "" H -3500 14250 50  0000 C CNN
	1    -3500 14250
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:R-midi-controler-rescue R?
U 1 1 5DCC213A
P -3500 14100
F 0 "R?" V -3420 14100 50  0000 C CNN
F 1 "10k" V -3500 14100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -3570 14100 50  0001 C CNN
F 3 "" H -3500 14100 50  0001 C CNN
	1    -3500 14100
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC2144
P -8050 13900
F 0 "#PWR?" H -8050 13650 50  0001 C CNN
F 1 "GND" H -8050 13750 50  0000 C CNN
F 2 "" H -8050 13900 50  0000 C CNN
F 3 "" H -8050 13900 50  0000 C CNN
	1    -8050 13900
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC214E
P -8000 15250
F 0 "#PWR?" H -8000 15000 50  0001 C CNN
F 1 "GND" H -8000 15100 50  0000 C CNN
F 2 "" H -8000 15250 50  0000 C CNN
F 3 "" H -8000 15250 50  0000 C CNN
	1    -8000 15250
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC2158
P -7950 16650
F 0 "#PWR?" H -7950 16400 50  0001 C CNN
F 1 "GND" H -7950 16500 50  0000 C CNN
F 2 "" H -7950 16650 50  0000 C CNN
F 3 "" H -7950 16650 50  0000 C CNN
	1    -7950 16650
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC2162
P -7900 18000
F 0 "#PWR?" H -7900 17750 50  0001 C CNN
F 1 "GND" H -7900 17850 50  0000 C CNN
F 2 "" H -7900 18000 50  0000 C CNN
F 3 "" H -7900 18000 50  0000 C CNN
	1    -7900 18000
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC216C
P -7850 20650
F 0 "#PWR?" H -7850 20400 50  0001 C CNN
F 1 "GND" H -7850 20500 50  0000 C CNN
F 2 "" H -7850 20650 50  0000 C CNN
F 3 "" H -7850 20650 50  0000 C CNN
	1    -7850 20650
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC2176
P -7900 19400
F 0 "#PWR?" H -7900 19150 50  0001 C CNN
F 1 "GND" H -7900 19250 50  0000 C CNN
F 2 "" H -7900 19400 50  0000 C CNN
F 3 "" H -7900 19400 50  0000 C CNN
	1    -7900 19400
	0    -1   -1   0   
$EndComp
Connection ~ -10050 19300
Wire Wire Line
	-9600 19300 -10050 19300
Connection ~ -10050 17900
Wire Wire Line
	-9600 17900 -10050 17900
Connection ~ -10050 16550
Wire Wire Line
	-9650 16550 -10050 16550
Connection ~ -10050 13800
Wire Wire Line
	-9750 13800 -10050 13800
Connection ~ -10050 12450
Wire Wire Line
	-9800 12450 -10050 12450
Connection ~ -10050 11100
Wire Wire Line
	-9800 11100 -10050 11100
Wire Wire Line
	-10050 11100 -10050 12050
Wire Wire Line
	-10050 12050 -10050 12450
Connection ~ -10050 12050
Wire Wire Line
	-10050 12050 -9100 12050
Connection ~ -10050 15150
Wire Wire Line
	-9700 15150 -10050 15150
Wire Wire Line
	-10050 16550 -10050 17500
Wire Wire Line
	-10050 17500 -10050 17900
Connection ~ -10050 17500
Wire Wire Line
	-8900 17500 -10050 17500
Wire Wire Line
	-10050 20550 -9550 20550
Wire Wire Line
	-8850 20550 -8950 20550
$Comp
L midi-controler-rescue:R-midi-controler-rescue R?
U 1 1 5DCC2198
P -9400 20550
F 0 "R?" V -9320 20550 50  0000 C CNN
F 1 "1k" V -9400 20550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9470 20550 50  0001 C CNN
F 3 "" H -9400 20550 50  0001 C CNN
	1    -9400 20550
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D?
U 1 1 5DCC21A2
P -9100 20550
F 0 "D?" H -9100 20650 50  0000 C CNN
F 1 "LED" H -9100 20450 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9100 20550 50  0001 C CNN
F 3 "" H -9100 20550 50  0001 C CNN
	1    -9100 20550
	-1   0    0    1   
$EndComp
NoConn ~ -8850 20750
Wire Wire Line
	-10050 19300 -10050 20150
Wire Wire Line
	-10050 20150 -10050 20550
Connection ~ -10050 20150
Wire Wire Line
	-10050 20150 -8850 20150
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW?
U 1 1 5DCC21B1
P -8350 20450
F 0 "SW?" H -8350 20900 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8350 20000 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8350 20450 60  0001 C CNN
F 3 "" H -8350 20450 60  0001 C CNN
	1    -8350 20450
	-1   0    0    1   
$EndComp
Wire Wire Line
	-8900 19300 -9000 19300
$Comp
L midi-controler-rescue:R-midi-controler-rescue R?
U 1 1 5DCC21BC
P -9450 19300
F 0 "R?" V -9370 19300 50  0000 C CNN
F 1 "1k" V -9450 19300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9520 19300 50  0001 C CNN
F 3 "" H -9450 19300 50  0001 C CNN
	1    -9450 19300
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D?
U 1 1 5DCC21C6
P -9150 19300
F 0 "D?" H -9150 19400 50  0000 C CNN
F 1 "LED" H -9150 19200 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9150 19300 50  0001 C CNN
F 3 "" H -9150 19300 50  0001 C CNN
	1    -9150 19300
	-1   0    0    1   
$EndComp
NoConn ~ -8900 19500
Wire Wire Line
	-10050 17900 -10050 18900
Wire Wire Line
	-10050 18900 -10050 19300
Connection ~ -10050 18900
Wire Wire Line
	-10050 18900 -8900 18900
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW?
U 1 1 5DCC21D5
P -8400 19200
F 0 "SW?" H -8400 19650 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8400 18750 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8400 19200 60  0001 C CNN
F 3 "" H -8400 19200 60  0001 C CNN
	1    -8400 19200
	-1   0    0    1   
$EndComp
Wire Wire Line
	-8900 17900 -9000 17900
$Comp
L midi-controler-rescue:R-midi-controler-rescue R?
U 1 1 5DCC21E0
P -9450 17900
F 0 "R?" V -9370 17900 50  0000 C CNN
F 1 "1k" V -9450 17900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9520 17900 50  0001 C CNN
F 3 "" H -9450 17900 50  0001 C CNN
	1    -9450 17900
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D?
U 1 1 5DCC21EA
P -9150 17900
F 0 "D?" H -9150 18000 50  0000 C CNN
F 1 "LED" H -9150 17800 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9150 17900 50  0001 C CNN
F 3 "" H -9150 17900 50  0001 C CNN
	1    -9150 17900
	-1   0    0    1   
$EndComp
NoConn ~ -8900 18100
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW?
U 1 1 5DCC21F5
P -8400 17800
F 0 "SW?" H -8400 18250 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8400 17350 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8400 17800 60  0001 C CNN
F 3 "" H -8400 17800 60  0001 C CNN
	1    -8400 17800
	-1   0    0    1   
$EndComp
Wire Wire Line
	-8950 16550 -9050 16550
$Comp
L midi-controler-rescue:R-midi-controler-rescue R?
U 1 1 5DCC2200
P -9500 16550
F 0 "R?" V -9420 16550 50  0000 C CNN
F 1 "1k" V -9500 16550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9570 16550 50  0001 C CNN
F 3 "" H -9500 16550 50  0001 C CNN
	1    -9500 16550
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D?
U 1 1 5DCC220A
P -9200 16550
F 0 "D?" H -9200 16650 50  0000 C CNN
F 1 "LED" H -9200 16450 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9200 16550 50  0001 C CNN
F 3 "" H -9200 16550 50  0001 C CNN
	1    -9200 16550
	-1   0    0    1   
$EndComp
NoConn ~ -8950 16750
Wire Wire Line
	-10050 15150 -10050 16150
Wire Wire Line
	-10050 16150 -10050 16550
Connection ~ -10050 16150
Wire Wire Line
	-10050 16150 -8950 16150
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW?
U 1 1 5DCC2219
P -8450 16450
F 0 "SW?" H -8450 16900 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8450 16000 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8450 16450 60  0001 C CNN
F 3 "" H -8450 16450 60  0001 C CNN
	1    -8450 16450
	-1   0    0    1   
$EndComp
Wire Wire Line
	-9000 15150 -9100 15150
$Comp
L midi-controler-rescue:R-midi-controler-rescue R?
U 1 1 5DCC2224
P -9550 15150
F 0 "R?" V -9470 15150 50  0000 C CNN
F 1 "1k" V -9550 15150 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9620 15150 50  0001 C CNN
F 3 "" H -9550 15150 50  0001 C CNN
	1    -9550 15150
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D?
U 1 1 5DCC222E
P -9250 15150
F 0 "D?" H -9250 15250 50  0000 C CNN
F 1 "LED" H -9250 15050 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9250 15150 50  0001 C CNN
F 3 "" H -9250 15150 50  0001 C CNN
	1    -9250 15150
	-1   0    0    1   
$EndComp
NoConn ~ -9000 15350
Wire Wire Line
	-10050 13800 -10050 14750
Wire Wire Line
	-10050 14750 -10050 15150
Connection ~ -10050 14750
Wire Wire Line
	-10050 14750 -9000 14750
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW?
U 1 1 5DCC223D
P -8500 15050
F 0 "SW?" H -8500 15500 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8500 14600 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8500 15050 60  0001 C CNN
F 3 "" H -8500 15050 60  0001 C CNN
	1    -8500 15050
	-1   0    0    1   
$EndComp
Wire Wire Line
	-9050 13800 -9150 13800
$Comp
L midi-controler-rescue:R-midi-controler-rescue R?
U 1 1 5DCC2248
P -9600 13800
F 0 "R?" V -9520 13800 50  0000 C CNN
F 1 "1k" V -9600 13800 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9670 13800 50  0001 C CNN
F 3 "" H -9600 13800 50  0001 C CNN
	1    -9600 13800
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D?
U 1 1 5DCC2252
P -9300 13800
F 0 "D?" H -9300 13900 50  0000 C CNN
F 1 "LED" H -9300 13700 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9300 13800 50  0001 C CNN
F 3 "" H -9300 13800 50  0001 C CNN
	1    -9300 13800
	-1   0    0    1   
$EndComp
NoConn ~ -9050 14000
Wire Wire Line
	-10050 12450 -10050 13400
Wire Wire Line
	-10050 13400 -10050 13800
Connection ~ -10050 13400
Wire Wire Line
	-10050 13400 -9050 13400
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW?
U 1 1 5DCC2261
P -8550 13700
F 0 "SW?" H -8550 14150 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8550 13250 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8550 13700 60  0001 C CNN
F 3 "" H -8550 13700 60  0001 C CNN
	1    -8550 13700
	-1   0    0    1   
$EndComp
Wire Wire Line
	-9100 12450 -9200 12450
$Comp
L midi-controler-rescue:R-midi-controler-rescue R?
U 1 1 5DCC226C
P -9650 12450
F 0 "R?" V -9570 12450 50  0000 C CNN
F 1 "1k" V -9650 12450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9720 12450 50  0001 C CNN
F 3 "" H -9650 12450 50  0001 C CNN
	1    -9650 12450
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D?
U 1 1 5DCC2276
P -9350 12450
F 0 "D?" H -9350 12550 50  0000 C CNN
F 1 "LED" H -9350 12350 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9350 12450 50  0001 C CNN
F 3 "" H -9350 12450 50  0001 C CNN
	1    -9350 12450
	-1   0    0    1   
$EndComp
NoConn ~ -9100 12650
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC2281
P -8100 12550
F 0 "#PWR?" H -8100 12300 50  0001 C CNN
F 1 "GND" H -8100 12400 50  0000 C CNN
F 2 "" H -8100 12550 50  0000 C CNN
F 3 "" H -8100 12550 50  0000 C CNN
	1    -8100 12550
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW?
U 1 1 5DCC228B
P -8600 12350
F 0 "SW?" H -8600 12800 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8600 11900 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8600 12350 60  0001 C CNN
F 3 "" H -8600 12350 60  0001 C CNN
	1    -8600 12350
	-1   0    0    1   
$EndComp
Wire Wire Line
	-9100 11100 -9200 11100
$Comp
L midi-controler-rescue:R-midi-controler-rescue R?
U 1 1 5DCC2296
P -9650 11100
F 0 "R?" V -9570 11100 50  0000 C CNN
F 1 "1k" V -9650 11100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V -9720 11100 50  0001 C CNN
F 3 "" H -9650 11100 50  0001 C CNN
	1    -9650 11100
	0    -1   -1   0   
$EndComp
$Comp
L midi-controler-rescue:LED-midi-controler-rescue D?
U 1 1 5DCC22A0
P -9350 11100
F 0 "D?" H -9350 11200 50  0000 C CNN
F 1 "LED" H -9350 11000 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H -9350 11100 50  0001 C CNN
F 3 "" H -9350 11100 50  0001 C CNN
	1    -9350 11100
	-1   0    0    1   
$EndComp
NoConn ~ -9100 11300
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC22AB
P -8100 11200
F 0 "#PWR?" H -8100 10950 50  0001 C CNN
F 1 "GND" H -8100 11050 50  0000 C CNN
F 2 "" H -8100 11200 50  0000 C CNN
F 3 "" H -8100 11200 50  0000 C CNN
	1    -8100 11200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	-10050 10700 -10050 11100
Wire Wire Line
	-10050 10300 -10050 10700
Connection ~ -10050 10700
Wire Wire Line
	-10050 10700 -9100 10700
Wire Wire Line
	-5550 20250 -7850 20250
Wire Wire Line
	-7900 19000 -5700 19000
Wire Wire Line
	-7900 17600 -6050 17600
Wire Wire Line
	-7950 16250 -6150 16250
Wire Wire Line
	-6200 14850 -8000 14850
Wire Wire Line
	-6600 13500 -8050 13500
Wire Wire Line
	-5650 12150 -8100 12150
Connection ~ -3500 13950
Wire Wire Line
	-3500 13750 -3500 13950
Wire Wire Line
	-3750 13750 -3500 13750
Wire Wire Line
	-2400 13400 -1800 13400
Wire Wire Line
	-2400 13550 -2400 13400
Wire Wire Line
	-3150 13850 -3150 15050
Wire Wire Line
	-2950 14250 -1800 14250
Wire Wire Line
	-2950 15700 -2950 14250
Wire Wire Line
	-5250 15700 -2950 15700
Wire Wire Line
	-5250 14750 -5250 15700
Wire Wire Line
	-5150 14750 -5250 14750
Wire Wire Line
	-3000 14150 -1800 14150
Wire Wire Line
	-3000 15650 -3000 14150
Wire Wire Line
	-5200 15650 -3000 15650
Wire Wire Line
	-5200 14850 -5200 15650
Wire Wire Line
	-5150 14850 -5200 14850
Wire Wire Line
	-3050 14050 -1800 14050
Wire Wire Line
	-3050 15600 -3050 14050
Wire Wire Line
	-5150 15600 -3050 15600
Wire Wire Line
	-5150 14950 -5150 15600
Wire Wire Line
	-3150 15050 -3750 15050
Wire Wire Line
	-4450 12400 -4450 13700
Wire Wire Line
	-4450 10300 -4450 12400
Connection ~ -4450 12400
Wire Wire Line
	-1800 12400 -4450 12400
Connection ~ -1800 13400
Wire Wire Line
	-1800 12400 -1800 13400
Wire Wire Line
	-5400 14650 -5400 15200
Wire Wire Line
	-5150 14650 -5400 14650
Connection ~ -3750 15050
Wire Wire Line
	-3750 14850 -3750 15050
Wire Wire Line
	-4350 15200 -3750 15200
Connection ~ -4350 15200
Wire Wire Line
	-4350 15200 -4350 15300
Wire Wire Line
	-5400 15200 -4450 15200
Wire Wire Line
	-4350 15200 -4450 15200
Connection ~ -4450 15200
Wire Wire Line
	-4450 15000 -4450 15200
Wire Wire Line
	-5550 14450 -5550 20250
Wire Wire Line
	-5150 14450 -5550 14450
Wire Wire Line
	-5700 14350 -5150 14350
Wire Wire Line
	-5700 19000 -5700 14350
Wire Wire Line
	-5150 14250 -6050 14250
Wire Wire Line
	-5150 14150 -6150 14150
Wire Wire Line
	-6200 14050 -5150 14050
Wire Wire Line
	-6200 14050 -6200 14850
Wire Wire Line
	-6600 13500 -6600 13950
Wire Wire Line
	-6600 13950 -5150 13950
Wire Wire Line
	-5650 13850 -5150 13850
Wire Wire Line
	-5650 12150 -5650 13850
Wire Wire Line
	-5550 10800 -5550 13750
Wire Wire Line
	-5550 13750 -5150 13750
$Comp
L midi-controler-rescue:SWITCH_DPDT-midi-controler-rescue SW?
U 1 1 5DCC22F4
P -8600 11000
F 0 "SW?" H -8600 11450 70  0000 C CNN
F 1 "SWITCH_DPDT" H -8600 10550 70  0000 C CNN
F 2 "slide_potentiometer:switch-(on)off" H -8600 11000 60  0001 C CNN
F 3 "" H -8600 11000 60  0001 C CNN
	1    -8600 11000
	-1   0    0    1   
$EndComp
$Comp
L midi-controler-rescue:Conn_01x01-midi-controler-rescue P?
U 1 1 5DCC22FE
P -1600 13950
F 0 "P?" H -1600 14050 50  0000 C CNN
F 1 "out" H -1600 13850 50  0000 C CNN
F 2 "Connectors:PINTST" H -1600 13950 50  0001 C CNN
F 3 "" H -1600 13950 50  0001 C CNN
	1    -1600 13950
	1    0    0    -1  
$EndComp
Wire Wire Line
	-2400 13850 -1800 13850
Wire Wire Line
	-3150 13850 -2400 13850
Connection ~ -2400 13850
$Comp
L midi-controler-rescue:C-midi-controler-rescue C?
U 1 1 5DCC230B
P -2400 13700
F 0 "C?" H -2375 13800 50  0000 L CNN
F 1 "100n" H -2375 13600 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L4.6mm_W3.8mm_P2.50mm_MKS02_FKP02" H -2362 13550 50  0001 C CNN
F 3 "" H -2400 13700 50  0001 C CNN
	1    -2400 13700
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:CONN_01X03-midi-controler-rescue P?
U 1 1 5DCC2315
P -1600 14150
F 0 "P?" H -1600 14350 50  0000 C CNN
F 1 "IN" V -1500 14150 50  0000 C CNN
F 2 "Connectors:Wafer_Vertical10x5.8x7RM2.5-3" H -1600 14150 50  0001 C CNN
F 3 "" H -1600 14150 50  0000 C CNN
	1    -1600 14150
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:CONN_01X02-midi-controler-rescue P?
U 1 1 5DCC231F
P -1600 13800
F 0 "P?" H -1600 13950 50  0000 C CNN
F 1 "alim" V -1500 13800 50  0000 C CNN
F 2 "Connectors:PINHEAD1-2" H -1600 13800 50  0001 C CNN
F 3 "" H -1600 13800 50  0000 C CNN
	1    -1600 13800
	1    0    0    -1  
$EndComp
Text Label -2200 12400 0    60   ~ 0
+5v2
$Comp
L midi-controler-rescue:GND-midi-controler-rescue #PWR?
U 1 1 5DCC232A
P -4350 15300
F 0 "#PWR?" H -4350 15050 50  0001 C CNN
F 1 "GND" H -4350 15150 50  0000 C CNN
F 2 "" H -4350 15300 50  0000 C CNN
F 3 "" H -4350 15300 50  0000 C CNN
	1    -4350 15300
	1    0    0    -1  
$EndComp
$Comp
L midi-controler-rescue:4051-midi-controler-rescue U?
U 1 1 5DCC2334
P -4450 14350
F 0 "U?" H -4350 14350 50  0000 C CNN
F 1 "4051" H -4350 14150 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_Socket_LongPads" H -4450 14350 60  0001 C CNN
F 3 "" H -4450 14350 60  0000 C CNN
	1    -4450 14350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
