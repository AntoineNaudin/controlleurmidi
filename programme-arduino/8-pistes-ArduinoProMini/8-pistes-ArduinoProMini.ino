// Map de connection entre les nammes signaux arduino -> cd4051 sliders
// arduino -->  sliders
// rouge   -->   blanc : 5v
// noir    -->   marron:  masse
// orange  --> bleu
// jaune   --> gris
// vert    --> violet



// Midi Signal Implementation
// |===============MESSAGE=================|============DONNEE1============|=========DONNEE2==========|
// | 1000 cccc = note off => 128(10) | 0xxx xxxx: hauteur note | 0xxx xxxx: vélocité |
// | 1001 cccc = note on => 144(10) | 0xxx xxxx: hauteur note | 0xxx xxxx: vélocité |
// | 1110 cccc = pitch bend => 224(10) | 0000 0000: code | 0xxx xxxx: vitesse |
// | 1011 cccc = control change => 176(10) | 0xxx xxxx: numero | 0xxx xxxx: valeur |
// --------------------------------------------------------------------------------------------------
//========================
#define NOTEOFF 128
#define NOTEON 144

#define CC 176

/*
Map sliders dans l'ordre (gauche = 0) :
index     0 1 2 3 4 5 6 7
which
sliders   0 5 3 7 6 2 4 1
fx1       4 6 7 5 2 1 0 3    
*/
uint8_t index_sliders[]={0,7,5,2,6,1,4,3};

uint8_t index_pcb[]={6,5,4,7,0,3,1,2};

/*
Midi implementation chart pour reason

channel Level: 1 2 3  4  5  6  7  8
Midi control :

Sliders        8  9  10 12 13 14 15 16  
Volume FX1     39        ...        46
       FX2     53 ....              60 
Mute           0  2  4  5  7  9  10 11 
Solo           24 26 28 29 31 33 35 36
               
*/
uint8_t map_reason_sliders[] = {  8,  9, 10, 12, 13, 14, 15, 16 };
uint8_t map_reason_fx1[]     = { 39, 40, 41, 42, 43, 44, 45, 46 };
uint8_t map_reason_fx2[]     = { 53, 54, 55, 56, 57, 58, 59, 60 };
uint8_t map_reason_mute[]    = {  0,  3,  4,  5,  7,  9, 10, 12 };  
uint8_t map_reason_solo[]    = { 24, 26, 28, 29, 31, 33, 35, 36 };


uint8_t which = 0;   //which y pin we are selecting
int val=0;

uint8_t values_sliders[]={0,0,0,0,0,0,0,0};
uint8_t min_values_sliders[]={0,0,0,0,0,0,0,0};
uint8_t max_values_sliders[]={0,0,0,0,0,0,0,0};

uint8_t values_fx1[]={0,0,0,0,0,0,0,0};
uint8_t min_values_fx1[]={0,0,0,0,0,0,0,0};
uint8_t max_values_fx1[]={0,0,0,0,0,0,0,0};

uint8_t values_fx2[]={0,0,0,0,0,0,0,0};
uint8_t min_values_fx2[]={0,0,0,0,0,0,0,0};
uint8_t max_values_fx2[]={0,0,0,0,0,0,0,0};


uint8_t values_solo[]={0,0,0,0,0,0,0,0};
uint8_t values_mute[]={0,0,0,0,0,0,0,0};


#define S0 11
#define S1 12
#define S2 13
#define IN_SLIDERS A7
#define IN_FX1 A3
#define IN_FX2  A2
#define IN_SOLO A0
#define IN_MUTE A1

#define PERCENT 4

void setup() {
  Serial.begin (9600);
  pinMode(IN_SLIDERS, INPUT);  
    pinMode(IN_FX1, INPUT);    
      pinMode(IN_FX2, INPUT);    
        pinMode(IN_SOLO, INPUT);    
          pinMode(IN_MUTE, INPUT);    // mux1 input 
  pinMode(S0, OUTPUT);    // s0
  pinMode(S1, OUTPUT);    // s1
  pinMode(S2, OUTPUT);    // s2
}  // end of setup

void MIDI_TX(unsigned char MESSAGE, unsigned char DONNEE1, unsigned char DONNEE2) //fonction d'envoi du message MIDI ==> voir tableau
{
 Serial.write(MESSAGE); //envoi de l'octet de message sur le port série
 Serial.write(DONNEE1); //envoi de l'octet de donnée 1 sur le port série
 Serial.write(DONNEE2); //envoi de l'octet de donnée 2 sur le port série
/*
Serial.print(MESSAGE); //envoi de l'octet de message sur le port série
Serial.print(" "); //envoi de l'octet de message sur le port série
 Serial.print(DONNEE1); //envoi de l'octet de donnée 1 sur le port sérieérie
Serial.print(" "); //envoi de l'octet de message sur le port série
 Serial.println(DONNEE2); //envoi de l'octet de donnée 2 sur le port série
 */
}

int buf=0,percent=0;

void loop(){

  ///////////////////////////////////////////////////////////////////////////////////
  //                              Selection canal                                  //                                           
  ///////////////////////////////////////////////////////////////////////////////////

  digitalWrite(S0, which&(1<<2));
  digitalWrite(S1, which&(1<<1));
  digitalWrite(S2, which&1);

  
  ///////////////////////////////////////////////////////////////////////////////////
  //                              Lecture Sliders                                  //                                           
  ///////////////////////////////////////////////////////////////////////////////////

  val = analogRead(IN_SLIDERS)/8;    
  if( val < min_values_sliders[which] || val > max_values_sliders[which]) {
    
    values_sliders[which] = val; 
    max_values_sliders[which] = val + (val*PERCENT/100+2); 
    
    if(val > 2) min_values_sliders[which] = val - (val*PERCENT/100+2);
    else min_values_sliders[which]=val;
    
    MIDI_TX((unsigned char) CC ,(unsigned char)(map_reason_sliders[index_sliders[which]]), (unsigned char)val);
  }
  
  
  ///////////////////////////////////////////////////////////////////////////////////
  //                                 Lecture fx1                                   //                                           
  ///////////////////////////////////////////////////////////////////////////////////

  val = analogRead(IN_FX1)/8;    
  if( val < min_values_fx1[which] || val > max_values_fx1[which]) {
    
    values_fx1[which] = val; 
    max_values_fx1[which] = val + (val*PERCENT/100+2); 
    
    if(val > 2) {
      min_values_fx1[which] = val - (val*PERCENT/100+2);
    }
    else {
      min_values_fx1[which]=val;
    }
    
    MIDI_TX((unsigned char) CC ,(unsigned char)map_reason_fx1[index_pcb[which]], (unsigned char)val);
  }
  
  ///////////////////////////////////////////////////////////////////////////////////
  //               Lecture fx2  Potard inversé, polarité aussi                     //                                           
  ///////////////////////////////////////////////////////////////////////////////////

  val =  127- (analogRead(IN_FX2)/8);    
  if( val < min_values_fx2[which] || val > max_values_fx2[which]) {
    
    values_fx2[which] = val; 
    max_values_fx2[which] = val + (val*PERCENT/100+2); 
    
    if(val > 2) {
      min_values_fx2[which] = val - (val*PERCENT/100+2);
    }
    else {
      min_values_fx2[which]=val;
    }
    
    MIDI_TX((unsigned char) CC ,(unsigned char)map_reason_fx2[index_pcb[which]], (unsigned char)val);
  }
  ///////////////////////////////////////////////////////////////////////////////////
  //                               Lecture Solo                                    //                                           
  ///////////////////////////////////////////////////////////////////////////////////
  val = digitalRead(IN_SOLO);    
  if( val != values_solo[which]) {
      MIDI_TX((unsigned char) NOTEON,(unsigned char)(map_reason_solo[index_pcb[which]]), (unsigned char)1);      
      MIDI_TX((unsigned char) NOTEOFF,(unsigned char)(map_reason_solo[index_pcb[which]]), (unsigned char)1);      
      values_solo[which] = val; 
  } 
  
  ///////////////////////////////////////////////////////////////////////////////////
  //                               Lecture Mute                                    //                                           
  ///////////////////////////////////////////////////////////////////////////////////
  val = digitalRead(IN_MUTE);    
  if( val != values_mute[which]) {
       MIDI_TX((unsigned char) NOTEON,(unsigned char)(map_reason_mute[index_pcb[which]]), (unsigned char)1);  
       MIDI_TX((unsigned char) NOTEOFF,(unsigned char)(map_reason_mute[index_pcb[which]]), (unsigned char)1);  
       values_mute[which] = val; 
  } 
  
  ///////////////////////////////////////////////////////////////////////////////////
  //                              Canal Suivant                                    //                                           
  ///////////////////////////////////////////////////////////////////////////////////

  which=(which+1)%8;

  
}  // end of loop
